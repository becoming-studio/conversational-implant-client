const structjson = require('./structjson.js');

var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

// Instantiate a DialogFlow client.
var dialogflow = require('dialogflow');
var sessionClient = new dialogflow.SessionsClient();

////////////////////////////////////

// You can find your project ID in your Dialogflow agent settings
const projectId = 'ci2-cqhpkq'; //https://dialogflow.com/docs/agents#settings
var sessionId = require('uuid/v1')();
var langCode = 'es';
var input_contexts;

////////////////////////////////////

/*
var cat = ["català", "catalan", "cat"];
var esp = ["castellano", "español", "spanish", "catella", "español", "esp"];
var eng = ["english", "inglés", "angles"];
*/

////////////////////////////////////

var temporizador;

app.get('/', function(req, res, next) {
	res.sendFile(__dirname + '/welcome_page/index.html')
});

app.get('/chat', function(req, res, next) {
	res.sendFile(__dirname + '/client/index.html')
});

app.use(express.static('client'));
app.use(express.static('welcome_page'));

////////////////////////////////////

io.on('connect', onConnect);


function onConnect(client){

	console.log('onconnect0... Client connected...');

	const dialogflow = require('dialogflow');

	// Instantiates a sessison client
	//const sessionClient = new dialogflow.SessionsClient();

	// The path to identify the agent that owns the created intent.
	const sessionPath = sessionClient.sessionPath(projectId, sessionId);

	// The text query request.
	const request = {
		session: sessionPath,
		queryInput: {
			event: {
				name: 'WELCOME',
				languageCode: langCode,
			},
		},
	};

	sessionClient
	.detectIntent(request)
	.then(responses => {
		logQueryResult(responses[0].queryResult);
		//console.log('Detected intent ' + responses[0].queryResult.intent.displayName);
		//console.log('fullfillmnet text ' + responses[0].queryResult.fulfillmentText);

		client.emit('pongu', responses[0].queryResult.fulfillmentText);
		/*temporizador = setTimeout(function(data){
		client.emit('empty');
		}, 10000);*/

	})
	.catch(err => {
		console.error('ERROR:', err);
	});


	// KEYBOARD INPUT HANDLER ////////////////////////////////////////////////////////
	client.on('mensa', function(query){
		console.log("MESSAGE: " + query);
		clearTimeout(temporizador);

		client.emit('pingu', query);

		// Define session path
		var sessionPath = sessionClient.sessionPath(projectId, sessionId);
		// The text query request.
		var request = {
			session: sessionPath,
			queryInput: {
				text: {
					text: query,
					languageCode: langCode,
				},
			},
			queryParams: {
				contexts: input_contexts,
			},
		};

		//console.log ("request ->");
		//console.log (request);

		// Send request and log result
		sessionClient
		.detectIntent(request)
		.then(responses => {

			const result = responses[0].queryResult;

			logQueryResult(result);

			//console.log('Detected intent ->');
			//console.log("query >> " + result.queryText);
			//console.log("response << "+ result.fulfillmentText);

			var parameters = JSON.stringify( structjson.structProtoToJson(result.parameters) );
			var parameters = JSON.parse(parameters);
			console.log("language querried : " + parameters.idioma);
			switch (parameters.idioma){
				case 'catala':
					langCode = 'fr';
					break;
			        case 'castellano':
					langCode = 'es';
					break;
				case 'english':
					langCode = 'en';
					break;
				default:
					langCode = langCode;
			}
			console.log("current  langcode : " + langCode);


			// aside language detection intent, store output conetxt to be used as input contexts in the next query
			result.outputContexts.forEach(context => {
				context.parameters = structjson.jsonToStructProto(
					structjson.structProtoToJson(context.parameters)
				);
			});
			input_contexts = result.outputContexts

			client.emit('pongu', result.fulfillmentText);

			temporizador = setTimeout(function(data){
				client.emit('empty');
			}, 30000);
		})
		.catch(err => {
			console.error('ERROR:', err);
		});
	});




	client.on('join', function(data) {
	console.log("JOIN: " + data);
	});

};

function logQueryResult(result) {
  // Imports the Dialogflow library
  const dialogflow = require('dialogflow');

  // Instantiates a context client
  const contextClient = new dialogflow.ContextsClient();

  console.log(`  Query: ${result.queryText}`);
  console.log(`  Response: ${result.fulfillmentText}`);
  if (result.intent) {
    console.log(`  Intent: ${result.intent.displayName}`);
  } else {
    console.log(`  No intent matched.`);
  }
  const parameters = JSON.stringify(
    structjson.structProtoToJson(result.parameters)
  );
  console.log('  Parameters: ' + parameters);
  if (result.outputContexts && result.outputContexts.length) {
    console.log(`  Output contexts:`);
    result.outputContexts.forEach(context => {
      const contextId = contextClient.matchContextFromContextName(context.name);
      const contextParameters = JSON.stringify(
        structjson.structProtoToJson(context.parameters)
      );
      console.log(`    ${contextId}`);
      console.log(`      lifespan: ${context.lifespanCount}`);
      console.log(`      parameters: ${contextParameters}`);
    });
  }
}

server.listen(7777);
